#include <Arduino.h>
#include "ros.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Bool.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

/**
 * Important constants
 */

#define SCREEN_WIDTH  128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

#define PULL_PIN 2        // Pin that the pull lead is connected to (PULLUP)

/**
 * The display object to control the OLED-display
 */
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

/**
 * Helper Funtions to better log text
 */

void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}

/**
 * Helper function to more easily wite text to the display
 * @param write     the text that should be written on the OLED
 * @param fontSize  Font Size for the written text
 */
void writeToDisplay(String write, int fontSize = 8) {
  display.clearDisplay();
  display.setTextSize(fontSize);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(write);
}

/**
 * Subscriber and Callback for displaying the points gained during the match
 */
void scoreCB(const std_msgs::UInt16 &msg_in) {
  loginfo(String("Score is now ") + msg_in.data);
  writeToDisplay(String(msg_in.data));
}
ros::Subscriber<std_msgs::UInt16> scoreSub("point_score", &scoreCB);

/**
 * Pubisher for activation system
 */
std_msgs::Bool pull_msg;
ros::Publisher pullPub("pull_to_start", &pull_msg);

/**
 * Pubishes the current state off the activation system
 */
void pullPinCB() {
  pull_msg.data = digitalRead(PULL_PIN);
  pullPub.publish(&pull_msg);
}

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   */
  nh.subscribe(scoreSub);
  nh.advertise(pullPub);

  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }

  /**
   * Initialize Display
   */
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x78)) {  // If an error occurs do not proceed
    nh.logfatal("SSD1306 allocation failed");
    while (true){delay(20000);} 
  }
  display.display();
  delay(2000);
  writeToDisplay(String(000));

  /**
   * PIN MODES
   */
  pinMode(PULL_PIN, INPUT_PULLUP);
}

/**
 * Will only execute ruoghly every 30ms the Pull Pin Publisher to not spam that much
 */
void loop() {
  nh.spinOnce();
  delay(10);

  nh.spinOnce();
  delay(10);

  nh.spinOnce();
  pullPinCB();
  delay(10);
}