#ifndef _ROS_controller_Arm_height_h
#define _ROS_controller_Arm_height_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace controller
{

  class Arm_height : public ros::Msg
  {
    public:
      typedef int16_t _heightMM_type;
      _heightMM_type heightMM;
      typedef bool _absolute_type;
      _absolute_type absolute;

    Arm_height():
      heightMM(0),
      absolute(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_heightMM;
      u_heightMM.real = this->heightMM;
      *(outbuffer + offset + 0) = (u_heightMM.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_heightMM.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->heightMM);
      union {
        bool real;
        uint8_t base;
      } u_absolute;
      u_absolute.real = this->absolute;
      *(outbuffer + offset + 0) = (u_absolute.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->absolute);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_heightMM;
      u_heightMM.base = 0;
      u_heightMM.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_heightMM.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->heightMM = u_heightMM.real;
      offset += sizeof(this->heightMM);
      union {
        bool real;
        uint8_t base;
      } u_absolute;
      u_absolute.base = 0;
      u_absolute.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->absolute = u_absolute.real;
      offset += sizeof(this->absolute);
     return offset;
    }

    const char * getType(){ return "controller/Arm_height"; };
    const char * getMD5(){ return "1183f2b9c81a282d6cb2f6f4d4f711e4"; };

  };

}
#endif