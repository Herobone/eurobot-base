#ifndef _ROS_controller_Arm_absolute_h
#define _ROS_controller_Arm_absolute_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace controller
{

  class Arm_absolute : public ros::Msg
  {
    public:
      typedef int16_t _angleLVL1_type;
      _angleLVL1_type angleLVL1;
      typedef int16_t _angleLVL2_type;
      _angleLVL2_type angleLVL2;
      typedef int16_t _dur_type;
      _dur_type dur;

    Arm_absolute():
      angleLVL1(0),
      angleLVL2(0),
      dur(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_angleLVL1;
      u_angleLVL1.real = this->angleLVL1;
      *(outbuffer + offset + 0) = (u_angleLVL1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_angleLVL1.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->angleLVL1);
      union {
        int16_t real;
        uint16_t base;
      } u_angleLVL2;
      u_angleLVL2.real = this->angleLVL2;
      *(outbuffer + offset + 0) = (u_angleLVL2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_angleLVL2.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->angleLVL2);
      union {
        int16_t real;
        uint16_t base;
      } u_dur;
      u_dur.real = this->dur;
      *(outbuffer + offset + 0) = (u_dur.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_dur.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->dur);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_angleLVL1;
      u_angleLVL1.base = 0;
      u_angleLVL1.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_angleLVL1.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->angleLVL1 = u_angleLVL1.real;
      offset += sizeof(this->angleLVL1);
      union {
        int16_t real;
        uint16_t base;
      } u_angleLVL2;
      u_angleLVL2.base = 0;
      u_angleLVL2.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_angleLVL2.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->angleLVL2 = u_angleLVL2.real;
      offset += sizeof(this->angleLVL2);
      union {
        int16_t real;
        uint16_t base;
      } u_dur;
      u_dur.base = 0;
      u_dur.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_dur.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->dur = u_dur.real;
      offset += sizeof(this->dur);
     return offset;
    }

    const char * getType(){ return "controller/Arm_absolute"; };
    const char * getMD5(){ return "a66eb4f6dce1b9a098fadf3b51c5391b"; };

  };

}
#endif